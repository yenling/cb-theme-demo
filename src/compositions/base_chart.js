
import { CBChart } from '@/libs/cb-chart/cb_chart.js'
import { CBChartDataArtist } from '@/libs/cb-chart/cb_data_artist.js'
import { CBLineChartPaint } from '@/libs/cb-chart/cb_chart_paint'

import { nextTick, onBeforeUnmount } from 'vue'

export const useBaseChart = (chart_id) => {
  let raw_data

  let chart

  const draw_chart = () => {
    chart = new CBChart(chart_id,
      {
        elem1: new CBChartDataArtist(raw_data, new CBLineChartPaint()),
      }
    )
  }

  const refresh_chart = (_raw_data) => {
    if (chart) {
      chart = chart.destroy()
      raw_data = []
    }

    if (!_raw_data) return

    raw_data = _raw_data

    nextTick(() => {
      if (!document.getElementById(chart_id)) {
        console.warn("The chart container does not exist.");
        return
      }
      draw_chart()
    })
  }

  const destroy = () => {
    if (chart) chart = chart.destroy()
  }

  // 在 bundle，DOM 元件內容不會在切換路由時被清除，所以這邊手動做
  onBeforeUnmount(() => {
    destroy()
  })

  return {
    refresh_chart,
  }
}
