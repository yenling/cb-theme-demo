import { reactive, readonly, provide } from 'vue'
// import { AccountRepository } from './repository'

// 因 router 會用到，UserStore 的 state 需在 function 外面
const state = reactive({
  id: undefined,
  username: undefined,
  first_name: undefined,
  mobile: undefined,
  email: undefined,

  user_permissions: null
})

const failed_promise = (response=50) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      reject(response)
    }, 1000)
  })
}

export const useUserStore = (permission_check_required) => {
  const register = (account_info) => {
    return failed_promise({
      message: '展示網頁無此功能',
      code: 500
    })
  }

  const login = (username, password) => {
    return failed_promise()
  }

  const verify = (permission_code) => {
    return failed_promise()
  }

  const logout = () => {
    return failed_promise()
  }

  const reset_pwd = (contact_info) => {
    return failed_promise(500)
  }

  const change_pwd = (data) => {
    if (!data["username"]) {
      data["username"] = state.username
    }
    return failed_promise({
      message: '展示網頁無此功能',
      code: 500
    })
  }

  const update_info = (username_type) => {
    let params = {
      username: state.username,
      first_name: state.first_name
    }
    if (username_type !== "mobile") {
      params.mobile = state.mobile
    }
    if (username_type !== "email") {
      params.email = state.email
    }
    return failed_promise({
      message: '展示網頁無此功能',
      code: 500
    })
  }

  const get_profile = () => {
    return failed_promise()
  }

  const send_verification = () => {
    return failed_promise()
  }

  const verify_verification = (code) => {
    return failed_promise()
  }

  const get_user_permissions = () => {
    if (!permission_check_required) return

    AccountRepository.user_permission.get()
      .then((payload) => {
        state.user_permissions = payload.data.map(perm => {
          return perm.codename
        })
      })
  }

  const has_permission = (perm_code) => {
    if (state.user_permissions) {
      return state.user_permissions.includes(perm_code)
    }
  }

  return {
    state: readonly(state),
    register,
    login,
    verify,
    logout,
    reset_pwd,
    change_pwd,
    update_info,
    get_profile,
    send_verification,
    verify_verification,
    get_user_permissions,
    has_permission
  }
}
