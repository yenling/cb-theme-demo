
import { computed, reactive } from '@vue/reactivity'
import { js_beautify } from 'js-beautify'


export const useDisplayOptions = (default_options, code_format) => {
  let display_options = {}
  for (let key in default_options) {
    if (default_options[key] && typeof default_options[key] === 'string' &&
    (!default_options[key].startsWith('{') || !default_options[key].endsWith('}')) &&
    (!default_options[key].startsWith('[') || !default_options[key].endsWith(']')) &&
    !default_options[key].includes(') =>')) {
      display_options[key] = `'${default_options[key]}'`
    }
    else {
      display_options[key] = `${default_options[key]}`
    }
  }

  const value_options = reactive(Object.assign({}, default_options))

  const demo_code = computed(() => {
    let code_options = ''
    for (let key in value_options) {
      if (value_options[key] &&
        value_options[key]?.toString() !== default_options[key]?.toString()
      ) {
        if (typeof value_options[key] === 'string' &&
        (!value_options[key].startsWith('{') || !value_options[key].endsWith('}')) &&
        (!value_options[key].startsWith('[') || !value_options[key].endsWith(']')) &&
        !value_options[key].includes('=>')) {
          code_options += `${key}: '${value_options[key]}', `
        } else {
          code_options += `${key}: ${value_options[key]}, `
        }
      }
    }
    return js_beautify(code_format(code_options))
  })

  return reactive({
    display_options,
    value_options,
    demo_code
  })
}
