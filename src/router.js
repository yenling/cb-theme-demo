import { createRouter, createWebHistory } from 'vue-router';

// 其中的 'text' 是給 navAside 辨認用的
export const MODULE_ROUTES = [
  {
    path: '/cb-theme/',
    component: () => import('./views/Root.vue'),
    children: [
      {
        path: '',
        name: 'CBThemeHome',
        redirect: { name: 'CBTIntro' },
      },
      {
        path: 'guide/',
        component: () => import('./views/Root.vue'),
        text: 'Giude',
        children: [
          {
            path: 'intro/',
            name: 'CBTIntro',
            component: () => import('./views/CBTheme/Guide/IntroPage.vue'),
            text: 'Getting Started',
          }
        ]
      },
      {
        path: 'components/',
        component: () => import('./views/Root.vue'),
        text: 'Components',
        children: [
          {
            path: 'select/',
            name: 'Select',
            component: () => import('./views/CBTheme/Components/SelectPage.vue')
          },
          {
            path: 'checkbox/',
            name: 'Checkbox',
            component: () => import('./views/CBTheme/Components/CheckboxPage.vue')
          },
          {
            path: 'radio/',
            name: 'Radio',
            component: () => import('./views/CBTheme/Components/RadioPage.vue')
          },
          {
            path: 'modal/',
            name: 'Modal',
            component: () => import('./views/CBTheme/Components/ModalPage.vue')
          },
          {
            path: 'button/',
            name: 'ButtonComponent',
            text: 'Button',
            component: () => import('./views/CBTheme/Components/ButtonPage.vue')
          },
        ]
      },
      {
        path: 'methods/',
        component: () => import('./views/Root.vue'),
        text: 'Methods',
        children: [
          {
            path: 'dialog/',
            name: 'Dialog',
            component: () => import('./views/CBTheme/Methods/DialogPage.vue')
          }
        ]
      },
      {
        path: 'styles/',
        component: () => import('./views/Root.vue'),
        text: 'Style',
        children: [
          {
            path: 'button/',
            name: 'Button',
            component: () => import('./views/CBTheme/Styles/ButtonPage.vue'),
          },
          {
            path: 'input/',
            name: 'Input',
            component: () => import('./views/CBTheme/Styles/InputPage.vue'),
          },
        ]
      },
    ]
  },

  {
    path: '/cb-account/',
    component: () => import('./views/Root.vue'),
    children: [
      {
        path: '',
        name: 'CBAccountHome',
        redirect: { name: 'CBAIntro' }
      },
      {
        path: 'guide/',
        component: () => import('./views/Root.vue'),
        text: 'Giude',
        children: [
          {
            path: 'intro/',
            name: 'CBAIntro',
            text: 'Getting Started',
            component: () => import('./views/CBAccount/Guide/IntroPage.vue'),
          }
        ]
      },
      {
        path: 'components/',
        component: () => import('./views/Root.vue'),
        text: 'Components',
        children: [
          {
            path: 'login/',
            name: 'LoginPage',
            text: 'Login',
            component: () => import('./views/CBAccount/Components/LoginPage.vue')
          },
          {
            path: 'register/',
            name: 'RegisterPage',
            text: 'Register',
            component: () => import('./views/CBAccount/Components/RegisterPage.vue')
          },
          {
            path: 'change_password/',
            name: 'ChangePasswordPage',
            text: 'ChangePassword',
            component: () => import('./views/CBAccount/Components/ChangePasswordPage.vue')
          },
          {
            path: 'verify_sms/',
            name: 'VerifySMSPage',
            text: 'VerifySMS',
            component: () => import('./views/CBAccount/Components/VerifySMSPage.vue')
          },
          {
            path: 'account_info/',
            name: 'AccountInfoPage',
            text: 'AccountInfo',
            component: () => import('./views/CBAccount/Components/AccountInfoPage.vue')
          },
        ]
      },
    ]
  },

  {
    path: '/cb-chart/',
    component: () => import('./views/Root.vue'),
    children: [
      {
        path: '',
        name: 'CBChartHome',
        redirect: { name: 'CBCIntro' }
      },
      {
        path: 'guide/',
        component: () => import('./views/Root.vue'),
        text: 'Giude',
        children: [
          {
            path: 'intro/',
            name: 'CBCIntro',
            text: 'Getting Started',
            component: () => import('./views/CBChart/Guide/IntroPage.vue'),
          }
        ]
      },
      {
        path: 'usage/',
        component: () => import('./views/Root.vue'),
        text: 'Usage',
        children: [
          {
            path: 'basic/',
            text: 'Basic',
            name: 'CBCUsageBasic',
            component: () => import('./views/CBChart/Usage/BasicPage.vue'),
          },
          {
            path: 'composition/',
            text: 'Composition',
            name: 'CBCComposition',
            component: () => import('./views/CBChart/Usage/CompositionPage.vue'),
          },
          {
            path: 'weather_chart/',
            text: 'WeatherChart',
            name: 'CBCWeatherChart',
            component: () => import('./views/CBChart/Usage/WeatherChartPage.vue'),
          }
        ]
      },
      {
        path: 'classes/',
        component: () => import('./views/Root.vue'),
        text: 'Classes',
        children: [
          {
            path: 'cb_chart/',
            text: 'CBChart',
            name: 'CBChartPage',
            component: () => import('./views/CBChart/Classes/CBChartPage.vue'),
          },
          {
            path: 'cb_data_artist/',
            text: 'CBDataArtist',
            name: 'CBDataArtistPage',
            component: () => import('./views/CBChart/Classes/CBDataArtistPage.vue'),
          },
          {
            path: 'cb_chart_paint/',
            text: 'CBChartPaint',
            name: 'CBChartPaintPage',
            component: () => import('./views/CBChart/Classes/CBChartPaintPage.vue'),
          },
          {
            path: 'cb_axis_paint/',
            text: 'CBAxisPaint',
            name: 'CBAxisPaintPage',
            component: () => import('./views/CBChart/Classes/CBAxisPaintPage.vue'),
          },
          {
            path: 'cb_weather_chart/',
            text: 'CBWeatherChart',
            name: 'CBWeatherChartPage',
            component: () => import('./views/CBChart/Classes/CBWeatherChartPage.vue'),
          }
        ]
      },
    ]
  }
]

const routes = [
  ...MODULE_ROUTES,
  {
    path: '/',
    name: 'Home',
    component: () => import('./views/Home.vue')
  },

  {
    path: '/login',
    name: 'Login',
    component: () => import('@/libs/cb-account/Login.vue')
  },
  {
    path: '/register',
    name: 'Register',
    component: () => import('@/libs/cb-account/Register.vue')
  },
  {
    path: '/register/verify_sms',
    name: 'VerifySMS',
    component: () => import('@/libs/cb-account/VerifySMS.vue')
  },
  {
    path: '/reset_password',
    name: 'ChangePassword',
    component: () => import('@/libs/cb-account/ChangePassword.vue')
  },
  {
    path: '/account/info/',
    name: 'AccountInfo',
    component: () => import('@/libs/cb-account/AccountInfo.vue')
  },
  {
    path: '/login/register_required/',
    name: 'LoginWithRegister',
    component: () => import('@/views/CBAccount/LoginWithRegister.vue')
  },
]

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    }
    if (to.hash) {
      // 回到頂端
      if (to.hash === '#') {
        return {
          top: 0,
          behavior: 'smooth',
        }
      }
      return {
        el: to.hash,
        top: 72,  // 64 (header 高度) + 一點 margin
        behavior: 'smooth',
      }
    }
    return { top: 0 }
  }
})

export default router
