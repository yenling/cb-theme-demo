import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

import 'highlight.js/lib/common';
import hljsVuePlugin from "@highlightjs/vue-plugin";

import './libs/cb-static/cb.util'
import './libs/cb-static/cb.date'
import './libs/cb-static/sprintf.min'
import '@/libs/cb-chart/index.css'

// 以下順序需固定
import 'virtual:windi-base.css';
import 'virtual:windi-components.css';
import cb_theme from './libs/cb-theme'
import './index.css'
import 'virtual:windi-utilities.css';

import './libs/cb-theme/base_font_icon/style.css'
import 'virtual:windi-devtools'

const app = createApp(App)

app.use(cb_theme)
app.use(router)
app.use(hljsVuePlugin)

app.mount('#app')
