// 帳號類別，帳號可能為手機或email或帳號
// 'username' | 'mobile' | 'email'
export const USERNAME_TYPE = 'username'

// 是否需要註冊及重設密碼功能
export const REGISTER_REQUIRED = false

// 用於讓 UserStore 內部判斷是否要去檢查 使用者是否有該頁面瀏覽權限
export const PERM_CHECK_REUIRED = false

// 註冊成功後是否需要驗證
export const VERIFICATION_REQUIRED = false

// 在帳號設定頁，信箱是否能修改
export const EMAIL_EDITABLE = false

// 在帳號設定頁，手機是否能修改
export const MOBILE_EDITABLE = false


// 登入成功後，重導向的預設頁面
// 若不需驗證，註冊成功後，重導向的首頁
// 手機驗證成功後，重導向的首頁
// 修改密碼成功後，重導向的頁面
export const HOME_PATH = {
  name: 'Home'
}

export const LOGIN_PATH = {
  name: 'Login'
}

// 點擊「註冊會員」時，導向的註冊頁面
export const REGISTER_PATH = {
  name: 'Register'
}

// 若需手機驗證，註冊成功後，重導向的手機驗證頁
// 登入回傳 403 時，導向的手機驗證頁
export const VERIFY_SMS_PATH = {
  name: 'VerifySMS'
}


